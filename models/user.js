'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bcrypt = require('bcrypt');

var UserSchema = new Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    pass: {
        type: String,
        required: false
    },
});

UserSchema.pre('save', function (next) {

    let u = this;

    if (!u.pass) return next();

    if (!u.isNew && !u.isModified('pass')) return next();

    const salt = bcrypt.genSaltSync();
    u.pass = bcrypt.hashSync(u.pass, salt);
    next();
});

UserSchema.methods.comparePass = function (password, callback) {
    return callback(bcrypt.compareSync(password, this.pass));
}

var User = mongoose.model('User', UserSchema);

module.exports = mongoose.model('User', UserSchema);
