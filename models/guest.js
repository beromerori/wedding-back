'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//var bcrypt = require('bcrypt');

var GuestSchema = new Schema({
    name: String,
    surnames: String,
    phone: {
        type: String,
        required: false
    },
    email: {
        type: String,
        //unique: true,
        required: false
    },
    intolerances: {
        type: String,
        required: false
    },
    hasBus: {
        type: Boolean,
        required: false
    },
    numBusSeats: {
        type: Number,
        required: false
    },
    hasChildMenu: {
        type: Boolean,
        required: false
    },
    partner: {
        type: Object,
        required: false
    },
    childs: {
        type: Array,
        required: false
    },
    // PARTNER / CHILDS
    guest_id: {
        type: String,
        ref: 'Guest',
        required: false
    }
});

GuestSchema.statics.findByFilters = function (filters, limit, callback) {

    var query = Guest.find(filters).select('-pass -__v');
    query.exec(callback);
}

var Guest = mongoose.model('Guest', GuestSchema);

module.exports = mongoose.model('Guest', GuestSchema);