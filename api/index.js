const app = require("express")();
const { v4 } = require("uuid");
const cors = require("cors");
const bodyParser = require("body-parser");
const logger = require("morgan");

const config = require("../lib/config");

// Connect to BBDD
require("../lib/connectMongoose");

app.get('/api', (req, res) => {
  const path = `/api/item/${v4()}`;
  res.setHeader('Content-Type', 'text/html');
  res.setHeader('Cache-Control', 's-max-age=1, stale-while-revalidate');
  res.end(`Hello! Go to item: <a href="${path}">${path}</a>`);
});

app.get('/api/item/:slug', (req, res) => {
  const { slug } = req.params;
  res.end(`Item: ${slug}`);
});

app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

const guestsRouter = require("../routes/api/guest");
app.use("/api/guests", guestsRouter);

module.exports = app;