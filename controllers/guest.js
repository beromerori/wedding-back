'use strict';

// CONST
const COMMON = require('../common/constants');

// Models
const Guest = require('../models/guest');

function getGuests(req, res) {

    Guest.findByFilters({}, 10, (error, guests) => {

        if (error) {

            return res.status(500).json({
                status: 500,
                message: COMMON.SERVER.ERRORS.USER.NOT_GUESTS
            });
        }

        res.status(200).json(guests);
    });
}

async function createGuest(req, res) {

    try {

        let guest = await addGuest(req.body);

        if (guest._id) {

            // Partner
            if (req.body.partner) {

                const partner = {
                    ...req.body.partner,
                    guest_id: guest._id
                };

                const newPartner = await addGuest(partner);

                guest = { ...guest._doc, partner: newPartner };
            }

            // Childs
            if (req.body.childs) {

                const list = req.body.childs.map(c => {

                    const child = {
                        ...c,
                        guest_id: guest._id
                    };

                    return addGuest(child);
                });

                const childList = await Promise.all(list);

                guest = guest.partner ? guest : guest._doc;
                guest = { ...guest, childs: childList };
            }

            // Update guest
            const newGuest = await updateGuest(guest);

            return res.status(201).json(newGuest);
        }
    }
    catch (error) {
        return res.status(error.status).json(error.message);
    }
}

function addGuest(body) {

    const guest = new Guest(body);

    return new Promise((resolve, reject) => {

        guest.save((error, newGuest) => {

            if (error && error.code !== 11000) {

                reject({
                    status: 500,
                    message: COMMON.SERVER.ERRORS.GUEST.CREATED
                });
            }

            if (error && error.code === 11000) {

                reject({
                    status: 400,
                    message: COMMON.SERVER.ERRORS.GUEST.ALREADY_CREATED
                });
            }

            resolve(newGuest);
        });
    });
}

function updateGuest(guest) {

    return new Promise((resolve, reject) => {

        const id = guest._id;
        const data = guest;

        Guest.findByIdAndUpdate(id, data, (error, updatedGuest) => {

            if (error) {

                reject({
                    status: 500,
                    message: COMMON.SERVER.ERRORS.GUEST.UPDATED
                });
            }


            if (!updatedGuest) {

                reject({
                    status: 404,
                    message: COMMON.SERVER.ERRORS.CAMP.NOT_FOUND
                });
            }

            resolve(updatedGuest);
        });
    });
}

module.exports = {
    getGuests,
    createGuest
};