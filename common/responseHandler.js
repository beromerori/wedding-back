'use strict';

var responseTypes = {
    200: (message) => { return { status: 200, message } },
    400: () => { return { status: 400, message: 'Bad credentials' } },
    401: () => { return { status: 401, message: 'Not authorized' } },
    403: () => { return { status: 403, message: 'Forbidden. Insufficient privileges' } },
    404: () => { return { status: 404, message: 'Not Found' } },
    409: () => { return { status: 409, message: 'Token expired' } },
    500: () => { return { status: 500, message: 'Internal Server Error' } },
    503: () => { return { status: 503, message: 'Invalid Token' } }
}

function response(status, message) {

    if (!message) message = 'OK';

    var responseTypesHandler = responseTypes[status];
    return responseTypesHandler(message);
}

module.exports = {
    response
};