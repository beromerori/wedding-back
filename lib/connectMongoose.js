'use strict';

const mongoose = require('mongoose');
const config = require('./config');

const options = {
  keepAlive: true,
  connectTimeoutMS: 30000,
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

mongoose.connect(config.db, options).then(
    function (success) {
        console.log('Connected to mongoDB');
    },
    function (error) {
        console.log('Failed connection to mongoDB', error);
        return;
    }
);