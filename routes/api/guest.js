'use strict';

const express = require('express');
const router = express.Router();

// Controller
const guestCtrl = require('../../controllers/guest');

router.get('/', guestCtrl.getGuests);
router.post('/', guestCtrl.createGuest);

module.exports = router;
