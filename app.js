const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const logger = require("morgan");

const config = require("./lib/config");

// Connect to BBDD
require('./lib/connectMongoose');

app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

const guestsRouter = require("./routes/api/guest");
app.use("/guests", guestsRouter);

app.listen(config.port, function () {
  console.log("Runnning on " + config.port);
});
module.exports = app;